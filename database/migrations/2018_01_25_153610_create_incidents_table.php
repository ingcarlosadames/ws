<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('_id');
            $table->string('kind')->nullable();
            $table->integer('locationId')->unsigned()->nullable();
            $table->boolean('isArchived')->nullable();
            $table->dateTimeTz('happenedAt')->nullable();
            $table->timestamps();
            $table->foreign('locationId')->references('_id')->on('localities')
                  ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
