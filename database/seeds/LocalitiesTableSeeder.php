<?php

use Illuminate\Database\Seeder;
use App\Localities;

class LocalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		$data = array(

				[
					'name' => 'Los Alcarrizos'

				],
				[
					'name' => 'Santo domingo'

				],

				[
					'name' => 'Los mina'

				]



			);

			Localities::insert($data);
    }
}
