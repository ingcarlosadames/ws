<?php

use Illuminate\Database\Seeder;
use App\Incidents;
use Carbon\Carbon;

class IncidentsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */  
               
    public function run()
    {
    	$now = Carbon::now();
    	$nowInLondonTz = Carbon::now(new DateTimeZone('America/Santo_Domingo'));
 

		$data = array(

				[
					'kind' => 'ROBBERY',
					'locationId' => 1,
					'happenedAt' => Carbon::now()->subMonth(),
					'isArchived' => false	

				],
				[
					'kind' => 'MURDER',
					'locationId' => 2,
					'happenedAt' => Carbon::now()->today(),
					'isArchived' => false	

				],

				[
					'kind' => 'SHOOTING',
					'locationId' => 3,
					'happenedAt' => Carbon::now()->today(),
					'isArchived' => false	

				],

				[
					'kind' => 'TRAFIC_ACCIDENT',
					'locationId' => 2,
					'happenedAt' => Carbon::now()->subDays(25),
					'isArchived' => false	

				],

				[
					'kind' => 'ASSAULT',
					'locationId' => 2,
					'happenedAt' => Carbon::now()->subdays(30),
					'isArchived' => false	

				]

			);

			Incidents::insert($data);

		}		
    
}
