<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'api'], function(){


	Route::post('users',[
		'as' => 'users.create',
		'uses' => 'Auth\AuthController@store'
		]);

	Route::post('users/signin',[
		'as' => 'users.signin',
		'uses' => 'Auth\AuthController@signin'
		]);
	
	Route::resource('incidents', 'IncidentsController',[
		'only' => ['index','store']
		]);

	Route::post('incidents/{incident}/archive',[
		'as' => 'incidents.archive',
		'uses' => 'IncidentsController@archive'
		]);

	Route::resource('localities', 'localitiesController',[
		'only' => ['index','show']
	]);


	Route::get('/', function () {

	return response()->json([
		    'error' => [

		    	'msg' => 'Forbidden'
		    ]
		        
		], 403);	

	});


});	