<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidents extends Model
{

	protected $table = "incidents";

	protected $dates = ['happenedAt', 'created_at, updated_at'];

	protected $dateFormat = 'Y-m-d\TH:i:sO';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id', 'kind', 'locationId', 'happenedAt', 'isArchived',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];



}
