<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Localities;
use JWTAuth;
//use Tymon\JWTAuth\Exceptions\JWTException;
//use Tymon\JWTAuth\Facades\JWTAuth;

class LocalitiesController extends Controller
{

    /**
     * $name represents the locality name
     * @var string
     */
    protected $name;

   /**
    * $localities represents the array of localities
    * @var array|collection
    */
   protected $localities; 

    /**
     * $locationId represents the incident's location identifier
     * @var int
     */
    protected $locationId;


    /**
     * $limit of records per page
     * @var int
     */
    protected $limit;

     /**
     * $sortBy sort by the specified column
     * @var int
     */
    protected $sortBy;

    /**
     * $sortByOrder represents the ascending or descending sort order
     * @var int
     */
    protected $sortByOrder;
    
    /**
     * $now represents the current datetime
     * @var datetime
     */
    protected $now;  



   public function __construct(Localities $localities){

        $this->middleware('jwt.auth');
        $this->localities = $localities;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (! $user = JWTAuth::parseToken()->authenticate()){

            return response()->json([
                'error' => [
                    'msg' => 'User not found']
            ], 404);
        }  

        $limit = $request->get('limit') ?: 10;

        $sortBy = $request->get('sortBy') ?: 'name';

        $sortByOrder = $request->get('order') ?: 'asc';

       //localities
       $localities = $this->localities->select('_id','name')->orderBy($sortBy , $sortByOrder)->paginate($limit);

        //validate if $localities array contains data
        if(!count($localities) > 0)
        {
 
          //If resource does not exists we return error message
          return response()->json([
                'error' => [
                    
                    'msg' => 'Localities not found'

                ]
            ], 404);

        }

        return response()->json([

                'localites' => $localities->all(),

                'paginator' => [


                    'total_count'  => $localities->total(),
                    'total_pages'  => $localities->lastPage(),
                    'current_page' => $localities->currentPage()
                ]


                
            ]
            ,200);
        
    }

 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        if (! $user = JWTAuth::parseToken()->authenticate()){

            return response()->json([
                'error' => [
                    'msg' => 'User not found']
            ], 404);
        }      
         
      //get location by id
        if(!$localities = $this->localities->find($id))
        {

           return response()->json([
                'error' => [
                    'msg' => 'Location not found'

                ]
            ], 404);
        }

        return response()->json([
            'msg' => 'location found',
            'location' => $localities
            ],200);              
    
    }





}
