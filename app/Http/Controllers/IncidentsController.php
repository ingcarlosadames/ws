<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Localities;
use App\Incidents;
use Carbon\Carbon;
use JWTAuth;

class IncidentsController extends Controller
{

    /**
     * $kind represents the incident's kind
     * @var string
     */
    protected $name;

    /**
     * $locationId represents the incident's location identifier
     * @var int
     */
    protected $locationId;

    /**
     * $happenedAt represents the date in which incident ocurred
     * @var datetimez
     */
    protected $happenedAt;

    /**
     * $incidents represents an array of locations 
     * @var array|collection|null
     */
    protected $incidents;

    /**
     * $kind incident's kind. One of [ROBBERY, MURDER, TRAFFIC_ACCIDENT, SHOOTING, ASSAULT]
     * @var collection
     */
    protected $kind;

    /**
     * $limit of records per page
     * @var int
     */
    protected $limit;

     /**
     * $sortBy sort by the specified column
     * @var int
     */
    protected $sortBy;

    /**
     * $sortByOrder represents the ascending or descending sort order
     * @var int
     */
    protected $sortByOrder;
    
    /**
     * $now represents the current datetime
     * @var datetime
     */
    protected $now;  


    public function __construct(Incidents $incidents, Localities $localities){

        $this->middleware('jwt.auth');
        $this->incidents = $incidents;
        $this->localities = $localities;
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if (! $user = JWTAuth::parseToken()->authenticate()){

            return response()->json([
                'error' => [
                    'msg' => 'User not found']
            ], 404);
        }          

        $limit = $request->get('limit') ?: 10;

        $sortBy = $request->get('sortBy') ?: 'kind';

        $sortByOrder = $request->get('order') ?: 'asc';

       //incidents within 30 days
       $incidents = $this->incidents->select('_id','kind','locationId',\DB::raw("DATE_FORMAT (happenedAt, '%Y-%m-%dT%TZ') as happenedAt"),\DB::raw('case isArchived when 1 then "true" when 0 then "false" end as isArchived'))->where('happenedAt', '>=', Carbon::now()->subMonth())->where('happenedAt', '<=', Carbon::now()->today())->where('isArchived','0')->orderBy($sortBy , $sortByOrder)->paginate($limit);

       //dd($incidents);


        //validate if $incidents array contains data
        if(!count($incidents) > 0)
        {
 
          //If resource does not exists we return error message
          return response()->json([
                'error' => [
                    
                    'msg' => 'Incident does not exist'

                ]
            ], 404);

        }

        return response()->json([

                'incidents' => $incidents->all(),

                'paginator' => [

                    'total_count'  => $incidents->total(),
                    'total_pages'  => $incidents->lastPage(),
                    'current_page' => $incidents->currentPage()
                ]


                
            ]
            ,200);
 
   
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         if (! $user = JWTAuth::parseToken()->authenticate()){

            return response()->json([
                'error' => [
                    'msg' => 'User not found']
            ], 404);
        }  

        //2012-02-28T20:27:21+0000
        $validator = \Validator::make($request->all(),
         [
            'kind' => 'required',
            'locationId' => 'required|integer',
            'happenedAt' => 'required|date_format:Y-m-d\TH:i:sO'
         ]);

        if ($validator->fails()) {
           
           return response()->json([

                'error' => [
                    'msg' => $validator->errors()
                ]
                
          ],422);

        }

        //collection of incidents
        $kind = $collection = collect(['ROBBERY', 'MURDER', 'TRAFFIC_ACCIDENT', 'SHOOTING', 'ASSAULT']);

        //validate if is a valid kind of incident
        if(!$kind->contains($request->get('kind')))
        {
            return response()->json([

                'error' => [
                    
                    'msg' => 'Invalid incident kind'

                ]
            ], 422);

        }

        //validate location exists
        $locationId = $this->localities->find($request->input('locationId'));

        if(!count($locationId) > 0)
        {

           return response()->json([
                'error' => [
                    'msg' => 'Invalid location'

                ]
            ], 422);
        }


        $data = [
            'kind' => $request->get('kind'),
            'locationId' => $request->get('locationId'),
            'happenedAt' => Carbon::parse($request->get('happenedAt')),//->toDate()->format(DATE_ATOM),
            'isArchived' => false

        ];

        //create incident 
        if($this->incidents->create($data))
        {
          
          return response()->json([
                'msg' => 'Incident created',
                'incident' => $data

            ], 201);
        }


        
    }



    /**
     * archives an incident.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request, $id)
    {

        if (! $user = JWTAuth::parseToken()->authenticate()){

            return response()->json([
                'error' => [
                    'msg' => 'User not found']
            ], 404);
        }  

       //get incident by id
        if(!$incidents = $this->incidents->find($id))
        {

           return response()->json([
                'error' => [
                    'msg' => 'Incident not found'

                ]
            ], 404);
        }


        //set isArchived status to true
        $incidents->isArchived = 1;
       
        if(!$incidents->save())
        {
            return response()->json([
                'error' => [
                    'msg' => 'Incident not found'

                ]
            ], 404);           

        }

        return response()->json([
            'msg' => 'Incident archived',
            'incident' => $incidents
            ],200);              
    
    }
 

}
