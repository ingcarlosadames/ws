<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localities extends Model
{

	 protected $table = 'localities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id', 'name'
    ];


}
