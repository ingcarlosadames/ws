## Webservice Installation Guide (use development branch)

The following commands will be executed in shell under the project�s root directory "webservice".


## Step 1

- Configure Homestead https://laravel.com/docs/5.4/homestead . Once you launch the vagrant box, connect via ssh and go to your projects folder. 


## Step 2

	git clone https://bitbucket.org/ingcarlosadames/ws/commits/branch/development webservice


## Step 3

	cd webservice

## Step 4

If not installed already, install composer. Then run command composer update in the projejct's root directory. This will install dependencies into the application. 

	composer update

## Step 5

Create the environment file into the project's root directory.

	touch .env	

Then, copy paste this content https://github.com/laravel/laravel/blob/master/.env.example into .env file


## Step 6

Generate application encryption key:

	php artisan key:generate


## Step 7

Run migrations to create database and dump dummy data.

	php artisan:migrate

	php artisan db:seed


## Step 8

Give  write access permissions to storage and bootstrap/cache directories.

	sudo chgrp -R www-data storage bootstrap/cache

	sudo chmod -R ug+rwx storage bootstrap/cache


## Step 9

Access to your application with your virtual machine IP addresss

	http://VM_MACHINE_IP_ADDRESS


And you�re ready to go!


